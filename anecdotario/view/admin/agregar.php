<?php require 'datos/top.php'; ?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php require 'datos/head.php'; ?>
</head>
<body class="grey lighten-2">
<?php require 'datos/menu.php'; ?>

<?php if (isset($_GET['ok'])) {
	echo '<script>alert("Se inserto Correctamente");</script>';
} ?>

<div class="container">
	<div class="row">
		<div class="col m3">
			<div class="card-panel">
				<a href="usuarios.php" class="btn waves-block grey darken-2">Volver</a>
			</div>
		</div>
		<div class="col m6">
			<div class="card-panel">
				<?php
					$tipoUser=$_GET['tipo'];
					switch ($tipoUser) {
						case 'admin':
				?>
					<form action="insertar.php?tipo=admin" method="POST">
						<h4><small>Llena todos los campos <span class="red-text">*</span></small></h4>
			
						<div class="input-field">
							<label for="nombre">Nombres</label>
							<input type="text" name="nombre" id="nombre" required>
						</div>
			
						<div class="input-field">
							<label for="apellido">Apellidos</label>
							<input type="text" name="apellido" id="apellido" required>
						</div>
			
						<div class="input-field">
							<label for="correo">Correo Electronico</label>
							<input type="email" name="correo" id="correo" required>
						</div>
			
						<div class="input-field">
							<label for="password">Contraseña</label>
							<input type="password" name="password" id="password" required>
						</div>
			
						<div class="input-field">
							<input type="submit" value="Agregar Administrador" class="btn grey darken-2">
						</div>
					</form>
				<?php
							break;
				
						case 'docente':
				?>
					<form action="insertar.php?tipo=docente" method="POST">
						<h4><small>Llena todos los campos <span class="red-text">*</span></small></h4>
			
						<div class="input-field">
							<label for="nombre">Nombres</label>
							<input type="text" name="nombre" id="nombre" required>
						</div>
			
						<div class="input-field">
							<label for="apellido">Apellidos</label>
							<input type="text" name="apellido" id="apellido" required>
						</div>
			
						<div class="input-field">
							<label for="correo">Correo Electronico</label>
							<input type="email" name="correo" id="correo" required>
						</div>
			
						<div class="input-field">
							<label for="password">Contraseña</label>
							<input type="password" name="password" id="password" required>
						</div>
			
						<div class="input-field">
							<input type="submit" value="Agregar Docente" class="btn grey darken-2">
						</div>
					</form>
				<?php						
							break;
				
						case 'estudiante':
				?>
					<form action="insertar.php?tipo=estudiante" method="POST">
						<h4><small>Llena todos los campos <span class="red-text">*</span></small></h4>
			
						<div class="input-field">
							<label for="nombre">Nombres</label>
							<input type="text" name="nombre" id="nombre" required>
						</div>
			
						<div class="input-field">
							<label for="apellido">Apellidos</label>
							<input type="text" name="apellido" id="apellido" required>
						</div>
			
						<div class="input-field">
							<label for="documento">Documento</label>
							<input type="number" name="documento" id="documento" required>
						</div>

						<div class="input-field">
							<select name="tipodocumento">
								<option disabled selected>Seleccione un Tipo Documento</option>
							<?php 
								require '../../login/conexion2.php';
								$sql = mysql_query("SELECT * FROM tipoDocumento",$con);

								while ($res=mysql_fetch_array($sql)) {
									$idgs=$res['idTipoDoc'];
									$nombres=$res['descripcion'];

									echo '<option value="'.$idgs.'">'.$nombres.'</option>';
								}
							 ?>
							</select>
						</div>

						<div class="input-field">
							<select name="g">
								<option disabled selected>Seleccione un Grado</option>
							<?php 
								require '../../login/conexion2.php';
								$sql = mysql_query("SELECT * FROM grados",$con);

								while ($res=mysql_fetch_array($sql)) {
									$idg=$res['idGrado'];
									$nombre=$res['nombre'];

									echo '<option value="'.$idg.'">'.$nombre.'</option>';
								}
							 ?>
							</select>
						</div>

						<div class="input-field">
							<label for="telefono">Telefono Fijo</label>
							<input type="text" name="telefono" id="telefono">
						</div>

						<div class="input-field">
							<label for="celular">Celular</label>
							<input type="text" name="celular" id="celular">
						</div>

						<div class="input-field">
							<label for="correo">Correo Electronico</label>
							<input type="email" name="correo" id="correo">
						</div>
			
						<div class="input-field">
							<input type="submit" value="Agregar Estudiante" class="btn grey darken-2">
						</div>
					</form>
				<?php		
						
							break;
				
						case 'grado':
				?>
					<form action="insertar.php?tipo=grado" method="POST">
						<h4><small>Llena todos los campos <span class="red-text">*</span></small></h4>
			
						<div class="input-field">
							<label for="nombre">Nombres Grado</label>
							<input type="text" name="nombre" id="nombre" required>
						</div>
			
						<div class="input-field">
							<input type="submit" value="Agregar Grado" class="btn grey darken-2">
						</div>
					</form>
				<?php		
						
							break;
						
						default:
							echo "algo malo ha pasado";
							break;
					}
				
				?>
				</div>
		</div>
	</div>
</div>

<?php require 'datos/footer.php'; ?>
</body>
</html>