<?php require 'datos/top.php'; ?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php require 'datos/head.php'; ?>
</head>
<body class="grey lighten-2">
<?php require 'datos/menu.php'; ?>

<div class="container">
	<div class="card-panel">
		<h3>Usuarios</h3>
		<h4><small>Que quieres hacer?</small></h4>
		<p>
			En esta pagina podras seleccionar si quieres agregar ya sea, estudiantes, docentes, o administradores. o ya sea ver los usuarios que ya estan regisrados.
		</p>
	</div>

	<div class="row">

		<div class="col m6">
			<div class="card-panel">
				<h4><small>Agregar Usuarios</small></h4>
			
				<a href="agregar.php?tipo=estudiante" class="btn waves-effect waves-light grey darken-2 waves-block">Agregar Estudiante</a><br>
			
				<a href="agregar.php?tipo=docente" class="btn waves-effect waves-light grey darken-2 waves-block">Agregar Docente</a><br>
			
				<a href="agregar.php?tipo=admin" class="btn waves-effect waves-light grey darken-2 waves-block">Agregar Administrador</a><br>
			</div>
		</div>

		<div class="col m6">
			<div class="card-panel">
				<h4><small>Lista de Usuarios</small></h4>
			
				<a href="lista.php?tipo=estudiante" class="btn waves-effect waves-light grey darken-2 waves-block">Ver Lista de Estudiantes</a><br>
			
				<a href="lista.php?tipo=docente" class="btn waves-effect waves-light grey darken-2 waves-block">Ver Lista de Docentes</a><br>
			
				<a href="lista.php?tipo=admin" class="btn waves-effect waves-light grey darken-2 waves-block">Ver Lista de Administradores</a><br>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col m6">
			<div class="card-panel">
				<h4><small>Agregar Grado</small></h4>

				<a href="agregar.php?tipo=grado" class="btn waves-effect waves-light grey darken-2 waves-block">Agregra Grado</a>
			</div>
		</div>

		<div class="col m6">
			<div class="card-panel">
				<h4><small>Lista Grados</small></h4>

				<a href="lista.php?tipo=grado" class="btn waves-effect waves-light grey darken-2 waves-block">Lista Grados</a>
			</div>
		</div>
	</div>
</div>

<?php require 'datos/footer.php'; ?>
</body>
</html>