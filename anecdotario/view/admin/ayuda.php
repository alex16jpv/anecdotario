<?php require 'datos/top.php'; ?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php require 'datos/head.php'; ?>
</head>
<body class="grey lighten-2">
<?php require 'datos/menu.php'; ?>

<div class="container">
	<h3>Bienvenido!!<small> nos alegra tenerte aqui</small></h3>
	<div class="card-panel">
		<h4><small>Que hacer?</small></h4>
		<p>
			Si es tu primera vez en esta plataforma mas abajo tienes un tutorial y unas instrucciones de lo contrario puedes navegar por las pestañas del menu superior.
		</p>

	</div>
	<div class="card-panel">
		<h4><small>Tutorial</small></h4>
		<div class="video-container">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/aNd6bjiFZBI?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
		</div>

	</div>
	<div class="card-panel">
		<h4><small>Instrucciones</small></h4>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</p>
	</div>
</div>

<?php require 'datos/footer.php'; ?>
</body>
</html>