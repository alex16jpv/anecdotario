<?php

  session_start();

  if($_SESSION['tipo'] == 1){
    header('location: ../view/admin/');
  }else if($_SESSION['tipo'] == 2){
    header('location: ../view/docente/');
  }else if($_SESSION['tipo'] == 3){
    header('location: ../view/estudiante/');
  }else if(!isset($_SESSION['tipo'])){
  	header('location: ../');
  }

 ?>
