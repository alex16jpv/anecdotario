<?php 

session_start();
if(isset($_SESSION['tipo'])){
	header('location: login/redirec.php');
}

 ?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Anecdotario</title>
	<link rel="stylesheet" href="assets/css/materialize.min.css">
	<link rel="stylesheet" href="login/sweetalert.css">
</head>
<body class="grey lighten-3">


<div class="container">
	<div class="row">
		<div class="col m3"></div>
		<div class="col m6">
			<div class="card-panel">
				<div class="input-field">
					<label for="user">Documento o Correo Electronico</label>
					<input type="text" id="user" autocomplete>
				
				</div>
				
				<div class="input-field">
					<label for="pass">Contraseña</label>
					<input type="password" id="pass" autocomplete>
				</div>
				
				<div class="input-field">
					<select name="tipo" id="tipo">
						<option value="3">Estudiante</option>
						<option value="2">Docente</option>
						<option value="1">Administrador</option>
					</select>
				</div>
				
				<div class="input-field">
					<a id="login" class="btn waves-effect waves-light grey darken-4 waves-block">Entrar</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/materialize.min.js"></script>
<script type="text/javascript" src="login/operaciones.js"></script>
<script type="text/javascript" src="login/sweetalert.min.js"></script>
<script type="text/javascript">
	$('select').material_select();
</script>
</body>
</html>