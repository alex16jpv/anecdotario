-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.25-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para anecdotario
CREATE DATABASE IF NOT EXISTS `anecdotario` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `anecdotario`;

-- Volcando estructura para tabla anecdotario.administradores
CREATE TABLE IF NOT EXISTS `administradores` (
  `idAdministrador` int(11) NOT NULL AUTO_INCREMENT,
  `nombreAdmin` varchar(50) NOT NULL DEFAULT '0',
  `apellidoAdmin` varchar(50) NOT NULL DEFAULT '0',
  `correo` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '0',
  `tipo` int(11) DEFAULT '1',
  PRIMARY KEY (`idAdministrador`),
  KEY `tipo` (`tipo`),
  CONSTRAINT `tipo` FOREIGN KEY (`tipo`) REFERENCES `tipo` (`idTipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla anecdotario.administradores: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `administradores` DISABLE KEYS */;
REPLACE INTO `administradores` (`idAdministrador`, `nombreAdmin`, `apellidoAdmin`, `correo`, `password`, `tipo`) VALUES
	(1, 'sadf', '0safsda', 'admin', 'admin', 1),
	(2, 'Alexander', 'Perez', 'alex16jpv@gmail.com', '41933788jlj', 1);
/*!40000 ALTER TABLE `administradores` ENABLE KEYS */;

-- Volcando estructura para tabla anecdotario.docentes
CREATE TABLE IF NOT EXISTS `docentes` (
  `idDocente` int(11) NOT NULL AUTO_INCREMENT,
  `nombreDocente` varchar(50) DEFAULT NULL,
  `apellidoDocente` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `tipo` int(11) DEFAULT '2',
  PRIMARY KEY (`idDocente`),
  KEY `tipog` (`tipo`),
  CONSTRAINT `tipog` FOREIGN KEY (`tipo`) REFERENCES `tipo` (`idTipo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla anecdotario.docentes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `docentes` DISABLE KEYS */;
REPLACE INTO `docentes` (`idDocente`, `nombreDocente`, `apellidoDocente`, `correo`, `password`, `tipo`) VALUES
	(1, 'gfdg', 'sdfaasd', 'docente', 'docente', 2);
/*!40000 ALTER TABLE `docentes` ENABLE KEYS */;

-- Volcando estructura para tabla anecdotario.estudiantes
CREATE TABLE IF NOT EXISTS `estudiantes` (
  `idEstudiante` int(11) NOT NULL AUTO_INCREMENT,
  `nombreEstudiante` varchar(50) DEFAULT NULL,
  `apellidoEstudiante` varchar(50) DEFAULT NULL,
  `documento` int(11) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `grado` int(11) DEFAULT NULL,
  `tipo` int(11) DEFAULT '3',
  PRIMARY KEY (`idEstudiante`),
  KEY `grados` (`grado`),
  KEY `tipos` (`tipo`),
  CONSTRAINT `grados` FOREIGN KEY (`grado`) REFERENCES `grados` (`idGrado`),
  CONSTRAINT `tipos` FOREIGN KEY (`tipo`) REFERENCES `tipo` (`idTipo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla anecdotario.estudiantes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `estudiantes` DISABLE KEYS */;
REPLACE INTO `estudiantes` (`idEstudiante`, `nombreEstudiante`, `apellidoEstudiante`, `documento`, `password`, `grado`, `tipo`) VALUES
	(1, NULL, NULL, 123, '123', NULL, 3);
/*!40000 ALTER TABLE `estudiantes` ENABLE KEYS */;

-- Volcando estructura para tabla anecdotario.grados
CREATE TABLE IF NOT EXISTS `grados` (
  `idGrado` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idGrado`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla anecdotario.grados: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `grados` DISABLE KEYS */;
REPLACE INTO `grados` (`idGrado`, `nombre`) VALUES
	(1, '1'),
	(2, '2'),
	(3, '3'),
	(4, '4');
/*!40000 ALTER TABLE `grados` ENABLE KEYS */;

-- Volcando estructura para tabla anecdotario.tipo
CREATE TABLE IF NOT EXISTS `tipo` (
  `idTipo` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idTipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla anecdotario.tipo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo` DISABLE KEYS */;
REPLACE INTO `tipo` (`idTipo`, `tipo`) VALUES
	(1, 'admin'),
	(2, 'docente'),
	(3, 'estudiante');
/*!40000 ALTER TABLE `tipo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
