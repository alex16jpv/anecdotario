<nav>
	<div class="nav-wrapper grey darken-4">
		<div class="container">
			<a href="./" class="brand-logo left">Admin - <?php echo $_SESSION['nombre']; ?></a>
			<ul class="right hide-on-med-and-down">
				<li><a href="./" class="waves-effect waves-light"><i class="material-icons left">home</i>Inicio</a></li>
				<li><a href="../../login/cerrarSesion.php"><i class="material-icons left">power_settings_new</i>Salir</a></li>
			</ul>
		</div>
	</div>
</nav>