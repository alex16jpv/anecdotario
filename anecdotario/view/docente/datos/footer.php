<footer class="page-footer grey darken-4">
	<div class="container">
		<div class="row">
			<div class="col l6 s12">
				<h5 class="white-text">Footer Content</h5>
				<p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
			</div>
			<div class="col l4 offset-l2 s12">
				<h5 class="white-text">Por aqui tambien se puede</h5>
				<ul>
					<li><a class="grey-text text-lighten-3" href="./">Inicio</a></li>
					<li><a class="grey-text text-lighten-3" href="estudiantes.php">Estudiantes</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Desconectarse</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright grey darken-3">
		<div class="container">
			© 2017 Copyright Institucion Educativa Libre Power by AnbuProTeam
		</div>
	</div>
</footer>


<script type="text/javascript" src="../../assets/js/jquery.js"></script>
<script type="text/javascript" src="../../assets/js/materialize.min.js"></script>
<script type="text/javascript">
	// $('.collapsible').collapsible();
	$('select').material_select();
</script>