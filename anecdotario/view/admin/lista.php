<?php require 'datos/top.php'; ?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php require 'datos/head.php'; ?>
</head>
<body class="grey lighten-2">
<?php require 'datos/menu.php'; ?>

<div class="container">
	<div class="row">
		<div class="col m3">
			<div class="card-panel">
				<a href="usuarios.php" class="btn waves-block grey darken-2">Volver</a>
			</div>
		</div>
		<div class="col m9">
			<div class="card-panel">
				<?php
					require '../../login/conexion2.php';
					$tipoUser=$_GET['tipo'];
					switch ($tipoUser) {
						case 'admin':
				?>
					<h4>Lista Administradores</h4>
					<table>
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Correo</th>
							</tr>
						</thead>
						<tbody>
				<?php
					$sql = mysql_query("SELECT * FROM administradores",$con);

					while ($res=mysql_fetch_array($sql)) {

						$nombre=$res['nombreAdmin'];
						$apellido=$res['apellidoAdmin'];
						$correo=$res['correo'];

						echo "<tr>";
						echo "<td>".$nombre."</td>";
						echo "<td>".$apellido."</td>";
						echo "<td>".$correo."</td>";
						echo "</tr>";
					}
				?>
						</tbody>
					</table>
				<?php
							break;
				
						case 'docente':
				?>
					<h4>Lista Docentes</h4>
					<table>
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Correo</th>
							</tr>
						</thead>
						<tbody>
				<?php
					$sql = mysql_query("SELECT * FROM docentes",$con);

					while ($res=mysql_fetch_array($sql)) {

						$nombre=$res['nombreDocente'];
						$apellido=$res['apellidoDocente'];
						$correo=$res['correo'];

						echo "<tr>";
						echo "<td>".$nombre."</td>";
						echo "<td>".$apellido."</td>";
						echo "<td>".$correo."</td>";
						echo "</tr>";
					}
				?>
						</tbody>
					</table>
				<?php				
							break;
				
						case 'estudiante':
				?>
					<h4>Lista Estudiantes</h4>
					<p class="red-text">Tenga en cuenta que esta no es toda la informacion de los estudiantes</p>
					<table>
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Documento</th>
								<!-- <th>Grado</th> -->
								<th>Telefono Fijo</th>
								<th>Celular</th>
								<!-- <th>Control</th> -->
							</tr>
						</thead>
						<tbody>
				<?php
					$sql = mysql_query("SELECT * FROM estudiantes",$con);

					while ($res=mysql_fetch_array($sql)) {

						$nombre=$res['nombreEstudiante'];
						$apellido=$res['apellidoEstudiante'];
						$documento=$res['documento'];
						// $grado=$res['grado'];
						$telefono=$res['telefono'];
						$celular=$res['celular'];

						echo "<tr>";
						echo "<td>".$nombre."</td>";
						echo "<td>".$apellido."</td>";
						echo "<td>".$documento."</td>";
						// echo "<td>".$grado."</td>";
						echo "<td>".$telefono."</td>";
						echo "<td>".$celular."</td>";
						// echo "<td><a href='#!'>Ver completo</a></td>";
						echo "</tr>";
					}
				?>
						</tbody>
					</table>
				<?php
						
							break;
				
						case 'grado':
				?>
					<h4>Lista Grados</h4>
					<table>
						<thead>
							<tr>
								<th>Grado</th>
							</tr>
						</thead>
						<tbody>
				<?php
					$sql = mysql_query("SELECT * FROM grados",$con);

					while ($res=mysql_fetch_array($sql)) {

						$nombre=$res['nombre'];

						echo "<tr>";
						echo "<td>".$nombre."</td>";
						echo "</tr>";
					}
				?>
						</tbody>
					</table>
				<?php
						
							break;
						
						default:
							echo "algo malo ha pasado";
							break;
					}
				
				?>
				</div>
		</div>
	</div>
</div>

<?php require 'datos/footer.php'; ?>
</body>
</html>