<?php

require_once('conexion.php');

class Usuario extends Conexion
{

	public function login($user, $pass, $tipo)
	{

		parent::conectar();

		$user  = parent::salvar($user);
		$pass = parent::salvar($pass);
		$tipo = parent::salvar($tipo);

		if ($tipo == 1) {
			$consulta = 'SELECT idAdministrador, nombreAdmin, apellidoAdmin, tipo from administradores where correo="'.$user.'" and password= "'.$pass.'"';


			$verificar_usuario = parent::verificarRegistros($consulta);

			if($verificar_usuario > 0){

				$user = parent::consultaArreglo($consulta);

				session_start();
				
				$_SESSION['id'] = $user['idAdministrador'];
				$_SESSION['nombre'] = $user['nombreAdmin'];
				$_SESSION['apellido'] = $user['apellidoAdmin'];
				$_SESSION['tipo']  = $user['tipo'];
				echo 'view/admin/';
			}else{
				echo 'error_3';
			}

			parent::cerrar();



		}elseif ($tipo == 2) {
			$consulta = 'SELECT idDocente, nombreDocente, apellidoDocente, tipo from docentes where correo="'.$user.'" and password= "'.$pass.'"';


			$verificar_usuario = parent::verificarRegistros($consulta);

			if($verificar_usuario > 0){

				$user = parent::consultaArreglo($consulta);

				session_start();
				
				$_SESSION['id'] = $user['idDocente'];
				$_SESSION['nombre'] = $user['nombreDocente'];
				$_SESSION['apellido'] = $user['apellidoDocente'];
				$_SESSION['tipo']  = $user['tipo'];
				echo 'view/Docente/';
			}else{
				echo 'error_3';
			}

			parent::cerrar();



		}elseif ($tipo == 3) {
			$consulta = 'SELECT idEstudiante, nombreEstudiante, apellidoEstudiante, tipo from estudiantes where documento="'.$user.'" and password= "'.$pass.'"';


			$verificar_usuario = parent::verificarRegistros($consulta);

			if($verificar_usuario > 0){

				$user = parent::consultaArreglo($consulta);

				session_start();
				
				$_SESSION['id'] = $user['idEstudiante'];
				$_SESSION['nombre'] = $user['nombreEstudiante'];
				$_SESSION['apellido'] = $user['apellidoEstudiante'];
				$_SESSION['tipo']  = $user['tipo'];
				echo 'view/Estudiante/';
			}else{
				echo 'error_3';
			}

			parent::cerrar();



		}
	}

}


?>
